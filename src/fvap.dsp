declare name 		"freeverb allpass approx";
declare author 		"jezar at dreampoint,  Grame";
declare license 	"BSD";
declare copyright 	"(c)GRAME 2006"; 


import("filter.lib");
import("effect.lib");
//import("maxmsp.lib");

line (value, time) = state ~ ( _ , _ ) : ! , _ 
	with {
		state (t , c) = nt , if( nt <= 0 , value , c + (value - c) / nt)
		with {
			nt = if( value != value' , samples, t - 1) ;
			samples = time * SR / 1000.0 ;
		} ;
	} ;

N  = hslider("ap1samps",2734,1,10000,1) ,600 : line;
aN = hslider("innerap1fb",0.25,-1,1,0.01);
fvap = (_,_ <: (*(aN),_:+:fdelay3(1<<14,N)), -) ~ _ : (!,_); 
   
process = fvap ;





